package com.ronaldvega.androidchat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.cometchat.pro.core.AppSettings;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.ronaldvega.androidchat.utils.Constants;

import static com.cometchat.pro.constants.CometChatConstants.Params.UID;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initCometChat();
        initViews();
    }

    private void initViews() {
        EditText userIdEditText = findViewById(R.id.userIdEditText);
        Button submitButton = findViewById(R.id.submitButton);

        submitButton.setOnClickListener(view -> {
            Log.d("ERROR", "********************************************************************************* ");
            if (CometChat.getLoggedInUser()==null){
                CometChat.login(userIdEditText.getText().toString(), Constants.KEY, new CometChat.CallbackListener<User>(){

                @Override
                public void onSuccess(User user) {
//                    redirectToGroupListScreen();
                    redirectToHomeScreen();
                    Log.d("SUCCESS", "Login Successful : " + user.toString());
                }

                    @Override
                public void onError(CometChatException e) {
                    Log.d("ERROR", "Login failed with exception: " + e.getMessage());
                }
            });
        }else {
                redirectToHomeScreen();
            }
    });
    }

    private void redirectToHomeScreen() {
        HomeMainActivity.start(this);
    }

    private void initCometChat() {

        AppSettings appSettings=new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(Constants.REGION).build();

        CometChat.init(this, Constants.APP_ID,appSettings, new CometChat.CallbackListener<String>() {
            @Override
            public void onSuccess(String successMessage) {
                Log.d("SUCCESS", "Comet Chat initialization completed successfully");
            }
            @Override
            public void onError(CometChatException e) {
                Log.d("ERROR", "Comet Chat initialization failed with exception: " + e.getMessage());
            }
        });
    }
}